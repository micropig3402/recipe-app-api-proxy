#!/bin/sh

# エラーがあったらシェルスクリプトをそこで打ち止めにしてくれる
set -e

envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;'